<!DOCTYPE html>
<html>
    <head>
        <title>Todos App</title>
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <meta id="token" name="_token" value="{{ csrf_token() }}">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
        <link href="css/app.css" rel="stylesheet" type="text/css">
    </head>
    <body v-cloak>

        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/main.js"></script>
    </body>
</html>
