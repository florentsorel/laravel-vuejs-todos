window.Vue = require('vue');
var VueRouter = require('vue-router');
import { configRouter } from './routes';

Vue.use(VueRouter);
Vue.use(require('vue-resource'));
Vue.http.options.root = '/api';
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('value');

const router = new VueRouter({
    history: true
});

configRouter(router);

const App = Vue.extend(require('./app.vue'));
router.start(App, 'body');