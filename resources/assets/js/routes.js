export function configRouter(router) {
    router.map({
        '*': {
            component: require('./components/layouts/404.vue')
        },
        '/todos': {
            component: require('./components/todos/index.vue')
        },
        '/about': {
            component: require('./components/about.vue')
        }
    });

    router.redirect({
        '/': '/todos'
    })
}