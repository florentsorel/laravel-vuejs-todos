<?php

Route::group(['middleware' => ['web']], function () {

    Route::get('/', function () {
        return view('index');
    });

    Route::get('{all}', function () {
        return View::make('index');
    });

    Route::group(['prefix' => 'api'], function() {
        Route::resource('todos', 'TodosController');
        Route::delete('/todos/destroy/completed', [
            'uses' => 'TodosController@destroyCompleted'
        ]);
    });
});
