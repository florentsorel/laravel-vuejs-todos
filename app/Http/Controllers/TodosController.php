<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Todo;
use App\Http\Requests;

class TodosController extends Controller
{

    protected $model;

    protected $request;

    public function __construct(Todo $model, Request $request)
    {
        $this->model = $model;
        $this->request = $request;
    }

    public function index()
    {
        return $this->model->all();
    }

    public function store()
    {
        return $this->model->create($this->request->all());
    }

    public function update($id)
    {
        $todo = $this->model->find($id);
        $update = $todo->update([
            'body'      => $this->request->get('body'),
            'completed' => $this->request->get('completed'),
        ]);

        if ($update) {
            return response()->json(['success' => true]);
        }
    }

    public function destroy($id)
    {
        $todo = $this->model->find($id);
        if ($todo->delete()) {
            return response()->json(['success' => true]);
        }
    }

    public function destroyCompleted()
    {
        $todos = [];
        foreach ($this->request->get('todos') as $todo) {
            $todos[] = $todo['id'];
        }
        $this->model->destroy($todos);
    }
}
