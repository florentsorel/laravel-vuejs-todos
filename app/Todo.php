<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Todo extends Model
{
    protected $casts = [
        'completed' => "boolean"
    ];

    protected $fillable = ['body', 'completed'];
}
